/*

    Copyright 2011 Cuong Le <metacuong@gmail.com>

    License is under GPLv2 <http://www.gnu.org/licenses/gpl-2.0.txt>

*/

#include <QtGui/QApplication>
#include <QDeclarativeContext>
#include <QUrl>

#include "qmlapplicationviewer.h"
#include "controller.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));
    QmlApplicationViewer viewer;

    app->setOrganizationName("cuonglb");
    app->setApplicationName("DropN9");

    Controller controller(0);

    QDeclarativeContext *context = viewer.rootContext();
    context->setContextProperty("controllerMIT", &controller);
    context->setContextProperty("Options", &controller.m_options);
    context->setContextProperty("folderListModel", controller.folder_model);
    context->setContextProperty("filesTransferModel", controller.filestransfer_model);

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setSource(QUrl("qrc:/qml/dropN9/main.qml"));
    viewer.showExpanded();

    return app->exec();
}
