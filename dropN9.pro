
folder_01.source = qml/dropN9/imgs
folder_01.target = /
DEPLOYMENTFOLDERS = folder_01

CONFIG += qdeclarative-boostable meegotouch
QT += network

SOURCES += \
    main.cc \
    controller.cc \
    libs/oauth.cc \
    libs/networkcontroller.cc \
    libs/json.cc \
    libs/droprestapi.cc \
    libs/listmodel.cc \
    libs/folderitem.cc \
    libs/filetransferitem.cc \
    libs/options.cc

include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qml/dropN9/validate.js \
    qml/dropN9/object.js \
    qml/dropN9/ToolBar.qml \
    qml/dropN9/OptionsPage.qml \
    qml/dropN9/main.qml \
    qml/dropN9/LoginPage.qml \
    qml/dropN9/HeaderBar.qml \
    qml/dropN9/FolderList.qml \
    qml/dropN9/FilesTransferPage.qml \
    qml/dropN9/FastScroll.qml \
    qml/dropN9/FastScroll.js \
    qml/dropN9/FastScrollStyle.qml \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

HEADERS += \
    controller.h \
    libs/oauth.h \
    libs/networkcontroller.h \
    libs/json.h \
    libs/droprestapi.h \
    libs/listmodel.h \
    libs/folderitem.h \
    libs/filetransferitem.h \
    libs/options.h

RESOURCES += \
    resources.qrc


































































