import QtQuick 1.1
import com.nokia.meego 1.0

Item {
    width: parent.width
    height: 70

    property alias title: h_title.text
    property alias titles: h_title_s.text
    property alias subtitle: h_subtilte.text
    property alias havemenu: h_menu.visible
    property alias havecurrentdir: h_subtilte.visible

    signal menus(int index)

    Rectangle {
        width: parent.width
        height: parent.height
        color:"#f2f2f2"
    }

    Rectangle {
        id: top_banner
        x: -1
        y: -1
        width: parent.width + 1
        height: 71
        color: "#1086d6"

        Text {
            id:h_title
            visible: !havecurrentdir
            anchors.verticalCenter: parent.verticalCenter
            x:20
            color: "#ffffff"
            font.family: "Nokia Pure Text Light"
            font.pixelSize: 30
        }

        Text {
            id: h_title_s
            visible: havecurrentdir
            y:8
            x:20
            color: "#ffffff"
            font.family: "Nokia Pure Text Light"
            font.pixelSize: 30
        }

        Text {
            id:h_subtilte
            y:42
            x:20
            font.family: "Nokia Pure Text Light"
            font.pixelSize: 22
            color:"blue"
        }

        Image {
            id: h_arrow
            visible: h_menu.visible
            anchors.verticalCenter: parent.verticalCenter
            source: "file:///opt/dropN9/imgs/icon-m-combobox-arrow.png"
            x: parent.width - h_arrow.width - 20
        }

        MouseArea {
            id: h_menu
            anchors.fill: parent

            onPressed: parent.color = "#a5a2a5"
            onCanceled: parent.color = "#1086d6"
            onReleased: parent.color = "#1086d6"

            onClicked: mainMenu.open()
        }

        z:1
    }

    ContextMenu {
        id: mainMenu
        anchors.top: parent.top
        MenuLayout {
            MenuItem {text: "Create new folder...";onClicked: menus(6);}
            MenuItem {text: "Upload file(s)...";onClicked: menus(5);}
            MenuItem {text: "Refresh";onClicked: menus(4);}
            MenuItem {text: "Account info...";onClicked: menus(3);}
            MenuItem {text: "Sign out";onClicked: menus(2);}
            MenuItem {text: "Options...";onClicked: menus(1);}
            MenuItem {text: "About...";onClicked: menus(0);}
        }
    }
}
