import QtQuick 1.1
import com.nokia.meego 1.0
import com.meego.extras 1.0
import Qt.labs.folderlistmodel 1.0

import "object.js" as Obj
import "validate.js" as Validate

import "FastScroll.js" as Sections

Page {
    id: folderList

    signal dialogClose

    property bool popup_dir: false
    property string m_file_folder_to_rename: ""
    property string m_file_folder_to_rename_path: ""
    property bool m_is_move: false
    property bool m_is_copy: false
    property int  m_single_index: -1
    property bool have_checked : false
    property string currentDir: "/"

    property variant pModel

    HeaderBar {
        id: top_banner
        titles: "DropN9"
        subtitle: currentDir
        havecurrentdir: true
        havemenu: false
        onMenus: menu_callback(index)
        z:1
    }

    Component {
        id: folderDeligate
        Item {
            width: folderList.width
            height: 60

            Rectangle {
                id:rec_main
                anchors.fill: parent
                anchors.leftMargin: -1
                anchors.rightMargin: -1
                anchors.bottomMargin: 0
                color: model.checked ? "#e0c7c7":"transparent"

                border.color: "#eadfdf"

                Image {
                    id: i_right
                    x:5
                    anchors.verticalCenter: parent.verticalCenter
                    source: model.is_dir ? "file:///opt/dropN9/imgs/folder.png" : "file:///opt/dropN9/imgs/file.png"
                }

                Label {
                    id: l_name
                    x: i_right.width + 10
                    y:5
                    text:{
                        var nameof = model.path.split("/")
                        var nameof1 = nameof[nameof.length - 1]

                        var maxlength = 30
                        if (folderList.width > folderList.height)
                            maxlength = 70

                        if (nameof1.length >= maxlength)
                            return nameof1.substring(0,maxlength-10) + " ... " + nameof1.substring(nameof1.length-10,nameof1.length)
                        else
                            return nameof1
                        }
                    font.pixelSize: 22
                    font.bold: model.is_dir
                    font.family: "Nokia Pure Text Light"
                    color:"black"
                }

                Label {
                    y: l_name.y + l_name.height + 7
                    x: i_right.width + 10
                    text: {
                        var dmodified = model.modified.split("+")[0]
                        if (model.is_dir)
                            return dmodified
                        else
                            return "<b>"+model.size + "</b> | "+ dmodified
                       }
                    font.pixelSize: 16
                    font.family: "Nokia Pure Text Light"
                    color:"#8f5555"
                }

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        if (controllerMIT.is_multi_selection() && !m_is_move && !m_is_copy){
                            if (model.checked){
                                parent.color = "transparent"
                                controllerMIT.setCheck(index, false)
                            }else{
                                parent.color = "#e0c7c7"
                                controllerMIT.setCheck(index, true)
                            }
                        }else{
                            if (model.is_dir){
                                b_indicator.visible = true; b_indicator_wrap.visible=true; appWindow.showToolBar = true
                                controllerMIT.setFolderRoot(model.path)
                                controllerMIT.getlistoffolder()
                            }
                        }
                    }

                    onDoubleClicked: {
                        if (controllerMIT.is_multi_selection() && !m_is_move && !m_is_copy){
                            if (model.is_dir){
                                b_indicator.visible = true; b_indicator_wrap.visible=true; appWindow.showToolBar = true
                                appWindow.showToolBar = false
                                controllerMIT.setFolderRoot(model.path)
                                controllerMIT.getlistoffolder()
                            }
                        }
                    }

                    onPressAndHold: {
                        if (!controllerMIT.is_multi_selection() && !m_is_move && !m_is_copy){
                            var nameof = model.path.split("/")
                            var nameof1 = nameof[nameof.length - 1]
                            m_file_folder_to_rename = nameof1
                            m_file_folder_to_rename_path = model.path
                            popup_dir = model.is_dir
                            pModel = model
                            m_single_index = index
                            cm_popup_menu.open()
                        }else
                            if (have_checked)
                                cm_popup_menu_mutil.open()
                    }

                    onPressed: {
                        if (!controllerMIT.is_multi_selection() || m_is_move)
                            parent.color = "#e0c7c7"
                    }

                    onCanceled: {
                        if (!controllerMIT.is_multi_selection() || m_is_move)
                            parent.color = "transparent"
                    }

                    onReleased: {
                        if (!controllerMIT.is_multi_selection() || m_is_move)
                            parent.color = "transparent"
                    }
                }//MouseArea end

                Connections {
                    target: controllerMIT
                    onSetcheckindexchanged : {
                        rec_main.color = "transparent"
                    }
                }
            }
        }
    }// end folderDeligate

    ListView {
        id: folderListView
        visible:false
        anchors.fill: parent
        anchors.topMargin: 71
        model: folderListModel
        delegate: folderDeligate
        cacheBuffer: 1000
        section.property: "section"
        FastScroll{
            id:f_fastscroll
            listView: folderListView
        }
    } // end folderListView

    Rectangle{
        id:this_folder_is_empty
        visible: false
        anchors.fill: parent
        color:"transparent"
        Label {
            anchors.centerIn: parent
            text:"This Folder is Empty"
            color: "grey"
            font.pixelSize: 40
            font.family: "Nokia Pure Text Light"
        }
    }

    Rectangle {
        id:r_networkerror
        visible: false
        anchors.fill: parent
        Button {
            id:r_button_x
            text:"Retry"
            anchors.centerIn: parent
            width:200
            font.family: "Nokia Pure Text Light"
            onClicked: {
                r_networkerror.visible = false
                b_indicator.visible = true
                b_indicator_wrap.visible=true
                appWindow.showToolBar = false
                controllerMIT.refresh_current_folder()
            }
        }
        Label {
            y:r_button_x.y - r_button_x.height - 30
            x:10
            width:parent.width - 10
            text:"There was a problem opening this page. It could be lost connection or a slow network. Check your connection or try again later."
            font.pixelSize: 20
            font.family: "Nokia Pure Text Light"
        }

    }

    ContextMenu {
        id: cm_popup_menu
        MenuLayout {
            MenuItem {text: "Rename...";onClicked: rename_file_or_folder() }
            MenuItem {text: "Download...";onClicked:{
                                controllerMIT.setCheck(m_single_index, true)
                                downloadFiles()
                            }}
            MenuItem {text: "Copy...";onClicked:{
                                controllerMIT.setCheck(m_single_index, true)
                                copyFilesFolders()
                            }}
            MenuItem {text: "Delete...";onClicked:{
                                controllerMIT.setCheck(m_single_index, true)
                                deleteFilesFolders()
                            }}
            MenuItem {text: "Move...";onClicked:{
                                controllerMIT.setCheck(m_single_index, true)
                                moveFilesFolders()
                            }}
            MenuItem {text: "Shares..."; onClicked: shares_file_or_folder() }
            MenuItem {text: "Properties..."; onClicked: properties_file_or_folder() }                       
        }
    }

    ContextMenu {
        id: cm_popup_menu_mutil
        MenuLayout {
            MenuItem {text: "Download..."; onClicked: downloadFiles() }
            MenuItem {text: "Copy..."; onClicked: copyFilesFolders()}
            MenuItem {text: "Delete..."; onClicked: deleteFilesFolders() }
            MenuItem {text: "Move..."; onClicked: moveFilesFolders() }
        }
    }


    BusyIndicator {
        id:b_indicator
        anchors.centerIn: parent
        running: true
        platformStyle: BusyIndicatorStyle { size: "large" }
        z:1
    }

    MouseArea {
        id: b_indicator_wrap
        anchors.fill: parent
        visible: false
        z:10
    }

    Connections {
        target: controllerMIT

        onFolderfinished : {
            r_networkerror.visible = false
            folderListView.visible = true
            b_indicator.visible = false; b_indicator_wrap.visible=false;
            appWindow.showToolBar = true
            tb_back.platformIconId = controllerMIT.isRootDir() ? "icon-m-toolbar-back-dimmed":"icon-m-toolbar-back"
            tb_back.enabled = !controllerMIT.isRootDir()
            f_fastscroll.test()
            top_banner.havemenu = true

            currentDir = controllerMIT.getcurrentdir()
            changeCurrentDir()

            this_folder_is_empty.visible = folderListModel.count ? false : true
            folderListView.visible = folderListModel.count ? true : false
        }

        onOrientation_changed: Obj.orientation_changed(folderList)

        onNetwork_error : { //error
            top_banner.havemenu = false
            b_indicator.visible = false; b_indicator_wrap.visible=false;
            appWindow.showToolBar = false
            folderListView.visible = false
            r_networkerror.visible = true
        }

        onNotification : {
            i_infobanner.text = notification
            i_infobanner.parent = folderList
            i_infobanner.show()
        }

        onDelete_selected_items_finished: {
            b_indicator.visible = false; b_indicator_wrap.visible=false; appWindow.showToolBar = true
            i_infobanner.text = result
            i_infobanner.parent = folderList
            i_infobanner.show()
        }

        onEnable_download_and_delete_button: {
            ti_delete.platformIconId = have_checked ? "icon-m-toolbar-delete" : "icon-m-toolbar-delete-dimmed"
            ti_delete.enabled = have_checked
            ti_download.platformIconId = have_checked ? "icon-m-toolbar-down" : "icon-m-toolbar-down-dimmed"
            ti_download.enabled = have_checked
            ti_move.platformIconId = have_checked ? "icon-m-toolbar-directory-move-to" : "icon-m-toolbar-directory-move-to-dimmed"
            ti_move.enabled = have_checked
            folderList.have_checked = have_checked
        }

        onStopTransfer: {
            if (!controllerMIT.is_push_notification()){
                i_infobanner.text = "Files transfer completed"
                i_infobanner.parent = folderList
                i_infobanner.show()
            }
            refreshDir()
        }

        onCreate_folder_finished: {
            b_indicator.visible = false; b_indicator_wrap.visible=false; appWindow.showToolBar = true
            i_infobanner.text = result
            i_infobanner.parent = folderList
            i_infobanner.show()
        }

        onRename_folder_finished: {
            b_indicator.visible = false; b_indicator_wrap.visible=false; appWindow.showToolBar = true
            i_infobanner.text = result
            i_infobanner.parent = folderList
            i_infobanner.show()
        }

        onMove_files_folders_finished: {
            b_indicator.visible = false; b_indicator_wrap.visible=false;
            appWindow.showToolBar = true
            m_is_move = false
            m_is_copy = false
            toolicon_show(true);
            i_infobanner.text = result
            i_infobanner.parent = folderList
            i_infobanner.show()
        }

        onShares_finished:{ //result
            b_indicator.visible = false; b_indicator_wrap.visible=false;appWindow.showToolBar = true
            if (!result){
                i_infobanner.text = "Could not create share link, try again later."
                i_infobanner.parent = folderList
                i_infobanner.show()
            }
        }

        onShares_metadata:{ //url, expire
            sharemetadatadlg.url = url
            sharemetadatadlg.open()
        }

        onAccountinfo_finished: { //result
            b_indicator.visible = false; b_indicator_wrap.visible=false;appWindow.showToolBar = true
        }

        onAccountinfo_metadata: { //result
            var val=[]
            for(var i=0; i<6;i++){
                if (i==0)
                    val.push(result['display_name'])
                else if(i==1)
                    val.push(result['email'])
                else if(i==2)
                    val.push(result['uid'])
                else if(i==3)
                    val.push(__convertToMB(result['quota_info']['shared']))
                else if(i==4)
                    val.push(__convertToMB(result['quota_info']['quota']))
                else
                    val.push(__convertToMB(result['quota_info']['normal']))
            }
            accountinfodlg.m_data = val
            accountinfodlg.open()
        }
    }


    Sheet {
        id:accountinfodlg
        acceptButtonText: "OK"
        property variant m_data : ["","","","","",""]
        function dataChanged(result){
            var val
            for(var i=0; i<6;i++){
                if (i==0)
                    val=result['display_name']
                else if(i==1)
                    val=result['email']
                else if(i==2)
                    val=result['uid']
                else if(i==3)
                    val=result['quota_info']['shared']
                else if(i==4)
                    val=result['quota_info']['quota']
                else
                    val=result['quota_info']['normal']

                m_data[i] = val
            }
        }
        content: Flickable {
            anchors.fill: parent
            contentWidth: parent.width
            contentHeight: mycolumn.height + mycolumn.y            
            Label {
                id:t_label
                x:10
                y:10
                text:"Account Information"
                font.pixelSize: 30
                font.family: "Nokia Pure Text Light"
                color:"grey"
            }

            Rectangle {
                y: 45
                color:"grey"
                height:1
                width: parent.width
            }

            Column {
                id:mycolumn
                y:55
                x:10
                spacing: 10
                Repeater {
                    model: ["Display name","Email","Uid","Shared","Quota","Used"]
                    Row {
                        spacing: 10
                        Label {
                            text: modelData
                            color:"black"
                            font.pixelSize: 23
                            font.family: "Nokia Pure Text Light"
                            width:150
                        }
                        Label {
                            text: accountinfodlg.m_data[model.index]
                            font.pixelSize: 23
                            font.bold: true
                            color: "blue"
                            font.family: "Nokia Pure Text Light"
                        }
                    }
                }
            }

        }
    }//End of accountinfodlg

    Sheet {
        id:sharemetadatadlg
        acceptButtonText: "Done"
        property alias url: lxxf.text
        content: Rectangle {
            color: "transparent"
            anchors.fill: parent
                    Text {
                        id: lxxa
                        text: "Share link:"
                        color: "black"
                        x:10
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 20
                        font.family: "Nokia Pure Text Light"
                        height: 40
                    }
                    TextField {
                        id:lxxf
                        y: lxxa.height - 10
                        x:10
                        font.pixelSize: 20
                        font.family: "Nokia Pure Text Light"
                        width: parent.width - 20
                    }
        }
    }

    Component.onCompleted: {
       Obj.orientation_changed(folderList)
        controllerMIT.getlistoffolder()
    }

    tools: commonTools

    ToolBarLayout {
            id:commonTools
            visible: true

            ToolIcon {
                id:tb_back
                platformIconId: controllerMIT.isRootDir() ? "icon-m-toolbar-back-dimmed":"icon-m-toolbar-back"
                enabled: !controllerMIT.isRootDir()
                onClicked: {
                    controllerMIT.backtoRootDir()
                    controllerMIT.getlistoffolder()
                }
            }
// MOVE
            ToolButton {
                id: tb_move_cancel
                anchors.verticalCenter: parent.verticalCenter
                text: "Cancel"
                visible: false
                width:155
                onClicked: moveFilesFolders_cancel()
            }

            ToolButton {
                id: tb_move_here
                anchors.verticalCenter: parent.verticalCenter
                text: "Move in here"
                visible: false
                width:155
                onClicked: moveFilesFolders_ok()
            }
//COPY
            ToolButton {
                id: tb_copy_cancel
                anchors.verticalCenter: parent.verticalCenter
                text: "Cancel"
                visible: false
                width:155
                onClicked: copyFilesFolders_cancel()
            }

            ToolButton {
                id: tb_copy_here
                anchors.verticalCenter: parent.verticalCenter
                text: "Copy in here"
                visible: false
                width:155
                onClicked: copyFilesFolders_ok()
            }
//END
            ToolIcon {
                id: tb_move_here_cfg
                visible: false
                enabled: false
            }

            ToolIcon {
                id: ti_download
                platformIconId: "icon-m-toolbar-down-dimmed"
                onClicked: downloadFiles()
                enabled: false
            }

            ToolIcon {
                id: ti_delete
                platformIconId: "icon-m-toolbar-delete-dimmed"
                onClicked: deleteFilesFolders()
                enabled: false
            }

            ToolIcon {
                id: ti_move
                platformIconId: "icon-m-toolbar-directory-move-to-dimmed"
                onClicked: moveFilesFolders()
                enabled: false
            }

            ToolIcon {
                id: ti_multiselection
                platformIconId: controllerMIT.is_multi_selection() ? "icon-m-toolbar-list" : "icon-m-toolbar-list-selected"
                onClicked: {
                    controllerMIT.setMultiSelection()
                    i_infobanner.text = "Multi-selection " + (controllerMIT.is_multi_selection() ? "enable" : "disable")
                    i_infobanner.parent = folderList
                    i_infobanner.show()
                    platformIconId = controllerMIT.is_multi_selection() ? "icon-m-toolbar-list" : "icon-m-toolbar-list-selected"

                    if (!controllerMIT.is_multi_selection()){
                        ti_delete.platformIconId = "icon-m-toolbar-delete-dimmed"
                        ti_delete.enabled = false
                        ti_download.platformIconId = "icon-m-toolbar-down-dimmed"
                        ti_download.enabled = false
                        ti_move.platformIconId = "icon-m-toolbar-directory-move-to-dimmed"
                        ti_move.enabled = false
                    }
                }
            }

            ToolIcon {
                id: ti_transferbox
                platformIconId: "icon-m-toolbar-callhistory"
                onClicked: fileTransferManagement()
            }
    }


    MouseArea {
        id: modal
        anchors.fill: parent
        visible: false
    }

    QueryDialog {
        id: deleteconfirmationDlg
        message: "Do you really want to delete selected item(s) ?"
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        onAccepted: {
            b_indicator.visible = true; b_indicator_wrap.visible=true; appWindow.showToolBar = false
            deleteconfirmationDlg.close()
            controllerMIT.delete_selected_items()
        }
        onRejected: {
            if(!controllerMIT.is_multi_selection())
                controllerMIT.setCheck(m_single_index, false)
            deleteconfirmationDlg.close()
        }
    }

    QueryDialog {
        id: signoutconfirmationDlg
        message: "Do you really want to sign out and exit ?"
        acceptButtonText: "Yes"
        rejectButtonText: "No"
        onAccepted: {
            pageStack.pop()
            controllerMIT.logout()
        }
        onRejected: {
            signoutconfirmationDlg.close()
        }
    }

    QueryDialog{
        id: aboutdlg
        titleText: "DropN9"
        message:"<b>Dropbox</b> client for N9<br>"
            + "Version: <b>0.0.8 beta<b><br>"
            + "License : GPL v3<br><br>"
            + "&copy; 2011 Cuong Le<br>metacuong@gmail.com"
        acceptButtonText: "OK"
    }

    QueryDialog{
        id:propertiesdlg
        titleText: "Properties"
        acceptButtonText: "OK"
    }

    Sheet {
        id:folderdlg
        property bool m_type: true
        property alias m_initval: t_create_new_folder.text
        rejectButtonText: "Cancel"
        acceptButtonText: "OK"
        content: Rectangle {
            anchors.fill: parent
            color: "transparent"
                    Text {
                        id: l_create_new_folder
                        text: folderdlg.m_type?"Create new folder":"Rename file/folder"
                        color: "black"
                        x:10
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 20
                        font.family: "Nokia Pure Text Light"
                        height: 40
                    }
                    TextField {
                        id:t_create_new_folder
                        y: l_create_new_folder.height - 10
                        x:10
                        font.pixelSize: 20
                        font.family: "Nokia Pure Text Light"
                        width: parent.width - 20
                    }
        }
        onAccepted: {
            if (Validate.is_not_empty(t_create_new_folder.text)){
                var t_create_new_folder1 = Validate.ltrim(t_create_new_folder.text)
                var t_create_new_folder2 = Validate.rtrim(t_create_new_folder1)
                if (Validate.is_folder_name_allowed(t_create_new_folder2)){
                    if (!folderdlg.m_type && (m_file_folder_to_rename == t_create_new_folder2))
                        failed("Please choose a different name.")
                    else {
                        if (!controllerMIT.is_duplicated_file_folder(t_create_new_folder2)){
                            if (folderdlg.m_type)
                                createNewFolder(t_create_new_folder2)
                            else
                                renameFolder(m_file_folder_to_rename, t_create_new_folder2)
                        }else
                            failed("The file/folder name is duplicated, please choose a different name.")
                    }
                }
                else failed('The following characters are not allowed by Dropbox: \\ / : ? * < > \ " |')
            }
            else
                failed("Please enter " + (m_type?"folder name.":"new folder name."))
        }

        function failed(text) {
            i_infobanner.text = text
            i_infobanner.parent = folderdlg
            i_infobanner.show()
            t_create_new_folder.focus = true
            folderdlg.open()
        }
    }

    Sheet {
        id:folderuploaddlg
        rejectButtonText: "Done"
        content: ListView {
            id: listview
            anchors.fill: parent

            ScrollDecorator { flickableItem: listview }

            FolderListModel {
                id: folderModel
                nameFilters: ["*.*"]
                sortField:FolderListModel.Type
                showDotAndDotDot: true
            }

            Component {
                id: fileDelegate
                Item {
                    width: folderuploaddlg.width
                    height: 80
                    Rectangle {
                        id:rec_main
                        anchors.fill: parent
                        anchors.leftMargin: -1
                        anchors.rightMargin: -1
                        anchors.bottomMargin: 0
                        color: "transparent"
                        border.color: "#eadfdf"

                        Image {
                            id: i_right
                            x:5
                            anchors.verticalCenter: parent.verticalCenter
                            source: folderModel.isFolder(index) ? "file:///opt/dropN9/imgs/folder.png" : "file:///opt/dropN9/imgs/file.png"
                        }

                        Label {
                            id: l_name
                            x: i_right.width + 10
                            anchors.verticalCenter:parent.verticalCenter
                            text: fileName
                            font.pixelSize: 24
                            font.bold : folderModel.isFolder(index)
                            font.family: "Nokia Pure Text Light"
                            color:"black"
                        }

                        Label {
                            visible:  !folderModel.isFolder(index)
                            x: i_right.width + 10
                            y:50
                            font.pixelSize: 22
                            font.family: "Nokia Pure Text Light"
                            color:"grey"
                            text: !folderModel.isFolder(index)?controllerMIT.get_file_size(folderModel.folder+"/"+fileName):""
                        }

                        MouseArea {
                            anchors.fill: parent

                            onClicked: {
                                folderModel.folder=folderModel.folder+"/"+fileName
                            }

                            onPressAndHold: {
                                if (folderModel.isFolder(index)) {
                                    i_infobanner.text = "Dropbox not allowed to upload a folder, please select a file"
                                    i_infobanner.parent = folderuploaddlg
                                    i_infobanner.show()
                                }else{

                                    var filename = folderModel.folder+"/"+fileName
                                    var newfilename = filename.substring(7,filename.length)

                                    if (controllerMIT.filesize_is_valid(newfilename)){
                                        controllerMIT.transfer(newfilename, false)
                                        i_infobanner.text = "Added: " + newfilename
                                        i_infobanner.parent = folderuploaddlg
                                        i_infobanner.show()
                                    }else
                                    {
                                        i_infobanner.text = "Error: Dropbox not allowed to upload file greater than 300MB, please select another file"
                                        i_infobanner.parent = folderuploaddlg
                                        i_infobanner.show()
                                    }
                                }
                            }

                            onPressed: {
                                    parent.color = "#e0c7c7"
                            }

                            onCanceled: {
                                    parent.color = "transparent"
                            }

                            onReleased: {
                                    parent.color = "transparent"
                            }
                        }//MouseArea end
                    }
                }
            }

            model: folderModel
            delegate: fileDelegate

        }//end of listview
    }

    InfoBanner {
        id: i_infobanner
        timerEnabled: true
        timerShowTime: 3000
        z:2
    }

    function newFolder(){
        folderdlg.m_type = true
        folderdlg.m_initval=""
        folderdlg.open()
    }

    function rename_file_or_folder(){
        folderdlg.m_initval=m_file_folder_to_rename
        folderdlg.m_type = false
        folderdlg.open()
    }

    function deleteFilesFolders(){
        deleteconfirmationDlg.open()
    }

    function uploadFiles(){
        folderuploaddlg.open()
    }

    function downloadFiles(){
        controllerMIT.downloadSelectedFiles();
    }

    function fileTransferManagement(){
        pageStack.push(Qt.resolvedUrl("FilesTransferPage.qml"))
    }

    function optionsPage(){
        pageStack.push(Qt.resolvedUrl("OptionsPage.qml"))
    }

    function aboutDialog(){
        aboutdlg.open()
    }

    function feedbackDialog(){

    }

    function refreshDir(){
        b_indicator.visible = true; b_indicator_wrap.visible=true; appWindow.showToolBar = true
        controllerMIT.refresh_current_folder()
    }

    function moveFilesFolders(){
        i_infobanner.text = "Choose a destination folder to move your item(s) to."
        i_infobanner.parent = folderList
        i_infobanner.show()
        controllerMIT.add_move_files_folders_to_cache()
        m_is_move = true
        toolicon_show(false)
    }

    function copyFilesFolders() {
        i_infobanner.text = "Choose a destination folder to copy your item(s) to."
        i_infobanner.parent = folderList
        i_infobanner.show()
        controllerMIT.add_copy_files_folders_to_cache()
        m_is_copy = true
        toolicon_show(false)
    }

    function copyFilesFolders_cancel(){
        controllerMIT.copy_files_folders_to_clear_cache()
        m_is_copy = false
        toolicon_show(true)
    }

    function copyFilesFolders_ok(){
        b_indicator.visible = true; b_indicator_wrap.visible=true;appWindow.showToolBar = false
        controllerMIT.start_copy_files_folders()
    }

    function moveFilesFolders_cancel(){
        controllerMIT.move_files_folders_to_clear_cache()
        m_is_move = false
        toolicon_show(true)
    }

    function moveFilesFolders_ok(){
        b_indicator.visible = true; b_indicator_wrap.visible=true;appWindow.showToolBar = false
        controllerMIT.start_move_files_folders()
    }

    function properties_file_or_folder(){
        var nameof = pModel.path.split("/")
        var nameof1 = nameof[nameof.length - 1]

        var dpath = ""

        if ((nameof.length - 1) === 1)
            dpath = "/"
        else{
            for(var i=0;i<nameof.length - 1;i++)
                if (nameof[i].length)
                    dpath = dpath + "/" + nameof[i]
        }


        propertiesdlg.message = "<b>"+((pModel.is_dir?"Folder":"File") + ":</b> ") + nameof1 +"<br>"
                + "<b>Parent:</b> " + dpath + "</br>"
                + (!pModel.is_dir?("<br><b>File size:</b> "+pModel.size):"")
                + "<br><b>Revision:</b> "+pModel.revision

        propertiesdlg.open()
    }

    function toolicon_show(v){
        ti_download.visible = v
        ti_delete.visible = v
        ti_move.visible = v
        ti_multiselection.visible = v
        ti_transferbox.visible = v

        tb_move_cancel.visible = !v && m_is_move
        tb_move_here.visible = !v && m_is_move

        tb_copy_cancel.visible = !v && m_is_copy
        tb_copy_here.visible = !v && m_is_copy

        tb_move_here_cfg.visible = !v
    }

    function shares_file_or_folder() {
        b_indicator.visible = true; b_indicator_wrap.visible=true;appWindow.showToolBar = false
        var p = pModel.path
        controllerMIT.start_shares(p)
    }

    function createNewFolder(foldername){
        b_indicator.visible = true; b_indicator_wrap.visible=true;appWindow.showToolBar = false
        controllerMIT.createnewfolder(foldername)
    }

    function renameFolder(oldfoldername, newfoldername) {
        b_indicator.visible = true; b_indicator_wrap.visible=true;appWindow.showToolBar = false
        controllerMIT.renamefileorfolder(oldfoldername, newfoldername)
    }

    function accountInfo() {
        b_indicator.visible = true; b_indicator_wrap.visible=true;appWindow.showToolBar = false
        controllerMIT.accountinfo()
    }

    function signOutNow(){
        if (!controllerMIT.is_transfer()){
            signoutconfirmationDlg.open()
        }else{
            i_infobanner.text = "Please complete the upload/download tasks in Files Transfer before Sign out."
            i_infobanner.parent = folderList
            i_infobanner.show()
        }
    }

    function changeCurrentDir(){
         var maxlength = 30
         if (folderList.width > folderList.height)
               maxlength = 70

         if (!currentDir.length) currentDir = "/"
            if (currentDir.length >= maxlength)
                 currentDir = currentDir.substring(0,maxlength-10) + " ... " + currentDir.substring(currentDir.length-10,currentDir.length)
    }

    function menu_callback(index){
        switch(index){
        case 0: aboutdlg.open();break;
        case 1: optionsPage();break;
        case 2: signOutNow();break;
        case 3: accountInfo();break;
        case 4: refreshDir();break;
        case 5: uploadFiles();break;
        case 6: newFolder();break;
        default: break;
        }
    }

    function __convertToMB(bytestr) {
        return Math.round((controllerMIT.parseDouble(bytestr)/1024)/1024).toString() + " MB"
    }
}
