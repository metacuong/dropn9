import QtQuick 1.1
import com.nokia.meego 1.0
import com.meego.extras 1.0 // Need to uncomment it to deploy

import "validate.js" as Validate

Page {
    id:loginPage

    orientationLock: PageOrientation.LockPortrait

    HeaderBar {
        id: top_banner
        havemenu:false
        havecurrentdir:false
        title: "DropN9"
    }

    Rectangle {
        id: r_title
        y: top_banner.height
        width: parent.width
        height: 38
        color: "transparent"

        Label {
            id: t_title
            color: "#185264"
            text: qsTr("Dropbox client for N9")
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.pixelSize: 25
            anchors.centerIn: parent
        }
    }

    // Need to uncomment them to deploy
    InfoBanner {
        id: i_errorbanner
        timerEnabled: true
        timerShowTime: 3000
    }


    Rectangle {
        id: r_break_line
        x: -1
        y: r_title.y + r_title.height + 3
        width: parent.width + 1
        color:"black"
        border.color: "grey"
        height: 1
    }

    Rectangle {
        id: r_login_zone
        y: r_title.y + r_title.height + 20
        width: parent.width
        color: "transparent"

        Row {
            id: r_login_signin_button

            Column {
                width:loginPage.width

                Button {
                    id: btn_login
                    font.family: "Nokia Pure Text Light"
                    text:"Sign in"
                    platformStyle: ButtonStyle{inverted:true}
                    width: 270
                    anchors.centerIn: parent

                    onClicked: {
                        enabled = false
                        controllerMIT.oauth_v1_request_token()
                    }
                }

            }

        }// end row r_login_signin_button button

        Row {
            id: r_verify_button
            y: r_login_signin_button.height + 20

            Column {
                width:loginPage.width

                Button {
                    id: btn_verify
                    font.family: "Nokia Pure Text Light"
                    text:"Verify"
                    platformStyle: ButtonStyle{inverted:true}
                    width: 270
                    anchors.centerIn: parent
                    enabled: false

                    onClicked: {
                        enabled = false
                        controllerMIT.oauth_v1_access_token()
                    }

                    }

                }

            }

        // end row r_login_signin_button button

    }// end rectangle r_login_zone

    Rectangle {
        id: r_break_line1
        x: -1
        y: 270
        width: parent.width + 1
        color:"black"
        border.color: "grey"
        height: 1
    }

    Rectangle {
        id: r_suggest_to_create_new_account
        y: parent.height - 80
        width: parent.width
        color: "transparent"

        Text{
            id: t_question
            text:"Don't have an account yet ?"
            width:parent.width
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
            font.family: "Nokia Pure Text Light"
        }

        Label{
            text:"<a href='#'>Create account here</a>"
            y: t_question.height + 5
            width:parent.width
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 22
            font.bold: true
            font.family: "Nokia Pure Text Light"

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://www.dropbox.com/register")
            }
        }
        z:1
    } // end r_suggest_to_create_new_account

    Connections {
        target: controllerMIT

        onNetwork_error : {
            i_errorbanner.text = error
            i_errorbanner.show()
        }

        onAuthenticate_finished : {
            pageStack.pop()
            pageStack.push(Qt.resolvedUrl("FolderList.qml"))
        }

        onOpen_oauth_authorize_page : {
            Qt.openUrlExternally("https://www.dropbox.com/1/oauth/authorize?oauth_token="+oauth_token)
            btn_verify.enabled = true
        }

    }

    Label {
        id: lb_guide
        width:parent.width
        height: 80
        y: 290
        text: "<a href='#'><b>Please Read This Guide Before 'Sign in'.</b></a>"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 18
        font.family: "Nokia Pure Text Light"
        MouseArea {
            anchors.fill: parent
            onClicked: Qt.openUrlExternally("http://dropn9.my-meego.org/login_guide.html")
        }
    }

    tools: ToolBarLayout {
        visible: true
    }

}
