import QtQuick 1.1
import com.nokia.meego 1.0
import com.meego.extras 1.0
import "object.js" as Obj

Page {
    id: filestransferPage

    HeaderBar {
        id: top_banner
        title: "Files Transfer"
        havemenu: false
        havecurrentdir:false
        z:1
    }

    InfoBanner {
        id: i_infobanner
        timerEnabled: true
        timerShowTime: 3000
        z:2
    }

    Component {
        id: filestransferDeligate
        Item {
            width: filestransferPage.width
            height: 60

            Rectangle {
                id:rec_main
                anchors.fill: parent
                anchors.leftMargin: -1
                anchors.rightMargin: -1
                anchors.bottomMargin: 0
                color: "transparent"

                border.color: "#eadfdf"

                Image {
                    id: i_right
                    x:5
                    anchors.verticalCenter: parent.verticalCenter
                    source: "file:///opt/dropN9/imgs/file.png"
                }

                Label {
                    id: l_name
                    x: i_right.width + 10
                    y:5
                    text: {
                        var filename1 = filename.split("/")
                        var filename2 = filename1[filename1.length-1]

                        var maxlength = 30
                        if (filestransferPage.width > filestransferPage.height)
                            maxlength = 70

                        if (filename2.length >= maxlength)
                            return filename2.substring(0,maxlength-10) + " ... " + filename2.substring(filename2.length-10,filename2.length)
                        else
                            return filename2
                    }
                    font.pixelSize: 20
                    color:"black"
                }

                Label {
                    id: l_status
                    y: l_name.y + l_name.height
                    x: i_right.width + 10
                    visible: is_finished || in_queue
                    text: {
                        if (is_finished)
                            return (completed ? "Completed":"Failed ("+(is_cancelled?"it was cancelled by user":"network error")+")")+" "+(completed?"("+date+")":"")
                        else {
                            var dropbox_path1 = dropbox_path
                            var maxlength = 30
                            if (filestransferPage.width > filestransferPage.height)
                                maxlength = 70
                            if (dropbox_path && dropbox_path.length >= maxlength)
                                dropbox_path1 = dropbox_path.substring(0,maxlength-10) + " ... " + dropbox_path.substring(dropbox_path.length-10,dropbox_path.length)

                            //return "Pending ("+(dropbox_path1?dropbox_path1:"/")+")"
                            return model.size+" | Pending..." //size of download or upload file
                        }

                    }
                    font.pixelSize: 18
                    color: completed ? "#8f5555" : "red"
                }

                ProgressBar {
                    id: pb_updown
                    y: l_name.y + l_name.height
                    x: i_right.width + 10
                    visible: !is_finished && !in_queue
                    width: parent.width - 120
                    minimumValue: 1
                    maximumValue: 100
                    indeterminate : progressing ? false : true
                    value: progressing
                }

                Label {
                    id: lb_updown_total
                    y: pb_updown.y+pb_updown.height
                    x: i_right.width + 10
                    visible: !is_finished && !in_queue
                    font.pixelSize: 18
                    text:""
                }

                Connections {
                    target: controllerMIT
                    onProgressBarChanged : {
                        pb_updown.value = percent
                        if (sent_received || speed){
                            lb_updown_total.text = (is_download?"Received: ":"Sent: ")+__sent_received_calculate(sent_received) + " | Speed: " +__speed_calculate(speed)
                        }else
                            lb_updown_total.text = ""
                    }
                }

                Image {
                    id: i_fstatus
                    x:parent.width - 40
                    anchors.verticalCenter: parent.verticalCenter
                    source: is_download?"file:///opt/dropN9/imgs/icon-s-transfer-download.png" : "file:///opt/dropN9/imgs/icon-s-transfer-upload.png"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        var dropbox_path1 = dropbox_path
                        if (!dropbox_path)
                            dropbox_path1 = "/"
                        item_transfer_detail_dlg.xtype = is_download ? "Download":"Upload"
                        item_transfer_detail_dlg.xsize = "Size: "+ size
                        item_transfer_detail_dlg.xfrom = is_download ? dropbox_path1 : filename
                        item_transfer_detail_dlg.xto = is_download ? filename : dropbox_path1
                        item_transfer_detail_dlg.open()
                    }

                    onPressed: {
                            parent.color = "#e0c7c7"
                    }

                    onCanceled: {
                            parent.color = "transparent"
                    }

                    onReleased: {
                            parent.color = "transparent"
                    }
                }
            }
        }
    }//end filestransferDeligate

    function __sent_received_calculate(bytes){
        if (bytes < 1024)
            return Obj.toFixed(bytes,2)+ " bytes"
        else
        if (bytes < 1024*1024)
            return Obj.toFixed((bytes/1024),2)+ " KB"
        else
            return Obj.toFixed(((bytes/1024)/1024),2) + " MB"
    }

    function __speed_calculate(bytes){
        if (bytes < 1024)
            return Obj.toFixed(bytes,2) + " bps"
        else
            if(bytes < 1024*1024)
                return Obj.toFixed((bytes/1024),2) + " Kbps"
        else
                return Obj.toFixed(((bytes/1024)/1024),2) + " Mbps"
    }


    ListView {
        id: filestransferListView
        anchors.fill: parent
        anchors.topMargin: 71
        model: filesTransferModel
        delegate: filestransferDeligate

        onCountChanged: {
            filestransferListView.visible = (filestransferListView.model.count !== 0)
            no_transfers.visible = !filestransferListView.model.count
        }

        ScrollDecorator{flickableItem:filestransferListView}
    }//end ListView

    Sheet {
        id:item_transfer_detail_dlg
        property alias xtype: lb_type.text
        property alias xfrom: lb_from.text
        property alias xto: lb_to.text
        property alias xsize: lb_size.text
        acceptButtonText: "OK"
        content: Flickable{
            anchors.fill: parent
            contentHeight: myc.width + myc.y
            contentWidth: parent.width

            Column {
                id:myc
                y:10
                spacing: 10
                width: parent.width-20
                anchors.horizontalCenter: parent.horizontalCenter
                //----------------------
                Label {
                    id:lb_type
                    font.pixelSize: 28
                    font.family: "Nokia Pure Text Light"
                }
                //----------------------
                Label {
                    text:"From:"
                    font.pixelSize: 20
                    font.family: "Nokia Pure Text Light"
                    font.bold: true
                }
                //----------------------
                Label {
                    id:lb_from
                    width: parent.width
                    color:"black"
                    font.pixelSize: 20
                    font.family: "Nokia Pure Text Light"
                }
                //----------------------
                Label {
                    text:"To:"
                    font.pixelSize: 20
                    font.family: "Nokia Pure Text Light"
                    font.bold: true
                }
                //----------------------
                Label {
                    id:lb_to
                    width: parent.width
                    color:"black"
                    font.pixelSize: 20
                    font.family: "Nokia Pure Text Light"
                }
                //----------------------
                Label {
                    id:lb_size
                    font.pixelSize: 20
                    font.family: "Nokia Pure Text Light"
                    font.bold: true
                }
                //----------------------
            }
        }
    }

    Rectangle{
        id:no_transfers
        visible: false
        anchors.fill: parent
        color:"transparent"
        Label {
            anchors.centerIn: parent
            text:"No Transfers"
            color: "grey"
            font.pixelSize: 40
            font.family: "Nokia Pure Text Light"
        }
    }

    Connections {
        target: controllerMIT

        onNothingtotransfer:{
            i_infobanner.text = "Nothing to transfer"
            i_infobanner.show()
        }

        onCan_not_created_dropbox_folder:{
            i_infobanner.text = "Can not create the folder " + foldername + ", please recheck and try again later."
            i_infobanner.show()
            buttons_state(false)
        }

        onStartTransfer: buttons_state(true)

        onStopTransfer:{
            buttons_state(false)
        }

        onStop_and_cancel_finished:{
            ti_cancel.enabled = true
        }

        onOrientation_changed: Obj.orientation_changed(filestransferPage)
    }

    Component.onCompleted: {
        Obj.orientation_changed(filestransferPage)
        buttons_state(controllerMIT.is_transfer())
    }

    tools: commonTools

    ToolBarLayout {
            id:commonTools
            visible: true

            ToolIcon {
                platformIconId: "icon-m-toolbar-back"
                onClicked: {
                    pageStack.pop()
                }
            }

            ToolIcon{
                id:ti_transfer_process
                platformIconId: "icon-m-toolbar-callhistory"
                onClicked: controllerMIT.start_transfer_process()
            }

            ToolIcon {
                id: ti_delete
                platformIconId:"icon-m-toolbar-delete"
                onClicked: {
                    controllerMIT.transfer_clear_log()
                    buttons_state(false)
                }
            }

            ToolIcon{
                id:ti_cancel
                platformIconId: "icon-m-toolbar-close"
                onClicked: {
                    enabled = false
                    controllerMIT.current_transfer_stop_and_cancel()
                }
            }
    } //end of commonTools

    function buttons_state(isTransfer) {
        ti_transfer_process.platformIconId = (!isTransfer && filestransferListView.model.count !== 0)?"icon-m-toolbar-callhistory":"icon-m-toolbar-callhistory-dimmed"
        ti_transfer_process.enabled = (!isTransfer && filestransferListView.model.count !== 0)

        ti_delete.platformIconId=(!isTransfer && filestransferListView.model.count !== 0)?"icon-m-toolbar-delete":"icon-m-toolbar-delete-dimmed"
        ti_delete.enabled =(!isTransfer && filestransferListView.model.count !== 0)

        ti_cancel.platformIconId = isTransfer?"icon-m-toolbar-close":"icon-m-toolbar-close-dimmed"
        ti_cancel.enabled = isTransfer
    }


}
