import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
    id: appWindow

    showToolBar : false

    Component.onCompleted: {
        if (!controllerMIT.need_authenticate()){
            pageStack.push(Qt.resolvedUrl("FolderList.qml"))
        }else{
            pageStack.push(Qt.resolvedUrl("LoginPage.qml"))
        }
    }
}
