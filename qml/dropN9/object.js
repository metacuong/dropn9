//Stackless functions

function localFolderDialog(parent, cancel_callback, ok_callback) {
    createObjectVL("LocalFolderDialog.qml", parent, cancel_callback, ok_callback)
}

function localFileDialog(parent, cancel_callback, ok_callback) {
    createObjectVL("LocalFileDialog.qml", parent, cancel_callback, ok_callback)
}

function createNewFolder(parent, cancel_callback, ok_callback){
    var dlg=createObjectVL("CreateNewFolderDialog.qml", parent, cancel_callback, ok_callback)
    dlg.m_type = true
}

function renameFolder(parent, cancel_callback, ok_callback, oldfoldername){
    var dlg=createObjectVL("CreateNewFolderDialog.qml", parent, cancel_callback, ok_callback)
    dlg.m_type = false
    dlg.m_initval = oldfoldername
}

// Helper functions

function createCom(qmlFile, parent) {
    var com = Qt.createComponent(qmlFile);
    return com.createObject(parent);
}

function createObjectVL(qmlFile, parent, cancel_callback, ok_callback){
    var dlg = createCom(qmlFile, parent);
    dlg.close.connect(cancel_callback);
    dlg.ok.connect(ok_callback)
    return dlg
}

function orientation_changed(self) {
    var __screen_orientation = Options.screen_orientation();
    orientation_changed_back(self,__screen_orientation);
}

function orientation_changed_back(self,__screen_orientation) {
    if (__screen_orientation === 0)
        self.orientationLock = PageOrientation.LockPortrait;
    else if (__screen_orientation == 1)
        self.orientationLock = PageOrientation.LockLandscape;
    else
        self.orientationLock = PageOrientation.Automatic;
}

function toFixed(value, precision) {
    var power = Math.pow(10, precision || 0);
    return String(Math.round(value * power) / power);
}
