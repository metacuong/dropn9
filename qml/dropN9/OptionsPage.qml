import QtQuick 1.1
import com.nokia.meego 1.0

import "object.js" as Obj

Page {
    id: optionsPage

    HeaderBar {
        id: top_banner
        havemenu:false
        havecurrentdir:false
        title: "Options"
        z:1
    }

    property bool __is_transfer_auto
    property bool __is_push_notification
    property int __screen_orientation

    Flickable{
        width: parent.width
        height: parent.height
        y:70
        contentHeight: x_col.height+10
        contentWidth: parent.width

        Column {
            y:10
            id:x_col
            spacing: 10
            width: parent.width-20
            anchors.horizontalCenter: parent.horizontalCenter
            //----------------------
            Label {
                text:"Screen orientation"
                font.pixelSize: 24
                font.family: "Nokia Pure Text Light"
            }
            ButtonRow {
                id:r_screen_orientation
                Button {
                    id:b_screen_portrait
                    text:"Portrait";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__screen_orientation = 0
                        Obj.orientation_changed_back(optionsPage,0)
                    }
                }
                Button {
                    id:b_screen_landscape
                    text:"Landscape";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__screen_orientation = 1
                        Obj.orientation_changed_back(optionsPage,1)
                    }
                }
                Button {
                    id:b_screen_auto
                    text:"Auto";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__screen_orientation = 2
                        Obj.orientation_changed_back(optionsPage,2)
                    }
                }
            }
            //----------------------
            Label {
                text:"Start transfer"
                font.pixelSize: 24
                font.family: "Nokia Pure Text Light"
            }
            ButtonRow {
                id:r_files_transfer
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    id:b_transfers_manual
                    text:"Manual";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__is_transfer_auto = false
                    }
                }
                Button {
                    id:b_transfers_auto
                    text:"Auto";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__is_transfer_auto = true
                    }
                }
            }
            //---------------------
            Label {
                text:"Complete transfer notification"
                font.pixelSize: 24
                font.family: "Nokia Pure Text Light"
            }
            ButtonRow {
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    id: b_push
                    text:"Push";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__is_push_notification = true
                    }
                }
                Button {
                    id: b_banner
                    text:"Banner";font.family: "Nokia Pure Text Light";font.pixelSize: 22
                    onClicked: {
                        optionsPage.__is_push_notification = false
                    }
                }
            }
            //---------------------
        }
    }

    Component.onCompleted: {
        __is_transfer_auto = Options.is_transfers_auto()
        __screen_orientation = Options.screen_orientation()
        __is_push_notification = Options.is_push_notification()

        if (__is_transfer_auto)
            b_transfers_auto.checked = true
        else
            b_transfers_manual.checked = true

        if (__is_push_notification)
            b_push.checked = true
        else
            b_banner.checked = true

        switch(__screen_orientation) {
            case 0:b_screen_portrait.checked=true;break;
            case 1:b_screen_landscape.checked=true;break;
            case 2:b_screen_auto.checked=true;break;
        }

        Obj.orientation_changed(optionsPage)
    }

    tools: commonTools
    ToolBarLayout {
            id:commonTools
            visible: true

            ToolIcon {
                platformIconId: "icon-m-toolbar-back"
                onClicked: {
                    Options.set_transfers_auto(optionsPage.__is_transfer_auto)
                    Options.set_screen_orientation(optionsPage.__screen_orientation)
                    Options.set_push_notification(optionsPage.__is_push_notification)
                    controllerMIT.commit_orientation();
                    pageStack.pop()
                }
            }
    }
}
